// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
var activeActivities = 0;

/**
 * Increment the Android activity count. If the waitTimer is running, cancel it
 */
Alloy.Globals.addActiveActivity = function() {
    activeActivities++;
    Ti.App.fireEvent("activity:added", {count: activeActivities});
};

/**
 *  Increment the Android activity count. If the count is less than 1, start the wait timer to
 *  see if another activity starts. If it doesn't fire an app-wide android:noactivities event
 */
Alloy.Globals.removeActiveActivity = function() {
    activeActivities--;
    Ti.App.fireEvent("activity:removed", {count: activeActivities});
};