Ti.App.addEventListener("activity:added", updateActivityCount);

function doClick(e) {
    Alloy.createController("window2").getView().open();
}

function onOpen(e) {
    Ti.API.info("INDEX window OPEN");
    $.__activity = this.getActivity();
    $.__activity.addEventListener("pause", onPause);
    $.__activity.addEventListener("resume", onResume);
}

function onClose() {
    Ti.API.info("INDEX window CLOSE");
    $.__activity.removeEventListener("pause", onPause);
    $.__activity.removeEventListener("resume", onResume);

    $.destroy();
}

function onPause() {
    Ti.API.info("INDEX PAUSED");
    Alloy.Globals.removeActiveActivity();
}

function onResume() {
    Ti.API.info("INDEX RESUMED");
    Alloy.Globals.addActiveActivity();
}

function updateActivityCount(e) {
    Ti.API.info("INDEX updateActivityCount. Count = "+e.count);
    $.indexActivityCount.text = "Active activities = "+e.count;
}

$.index.open();
