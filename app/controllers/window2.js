

function close(e) {
    $.window2.close();
}

function onOpen(e) {
    Ti.API.info("WINDOW2 window OPEN");
    Ti.App.addEventListener("activity:added", updateActivityCount);
    $.__activity = this.getActivity();
    $.__activity.addEventListener("pause", onPause);
    $.__activity.addEventListener("resume", onResume);
}

function onClose() {
    Ti.API.info("WINDOW2 window CLOSE");
    Ti.App.removeEventListener("activity:added", updateActivityCount);
    $.__activity.removeEventListener("pause", onPause);
    $.__activity.removeEventListener("resume", onResume);

    $.destroy();
}

function onPause() {
    Ti.API.info("WINDOW2 PAUSED");
    Alloy.Globals.removeActiveActivity();
}

function onResume() {
    Ti.API.info("WINDOW2 RESUMED");
    Alloy.Globals.addActiveActivity();
}

function updateActivityCount(e) {
    Ti.API.info("WINDOW2 updateActivityCount. Count = "+e.count);
    $.win2ActivityCount.text = "Active activities = "+e.count;
}
