/**
 * Handle Android specific functionality. Currently only provides a way
 * to monitor the number of active Android activities and fires an app-wide
 * event, if the count reaches 0.
 *
 * @class lib.android
 */

var Android = {},
    waitTimer,
    monitorAndroidActivities = false;

/**
 * @property {Boolean} debug Enabling debugging output
 */
Android.debug = true;

/**
 * @property {Number} activeActivities The current number of active Android activities
 */
Android.activeActivities = 0;

/**
 * Start the Android activity counter
 */
Android.startActivityCounter = function() {
    Android.debug && console.debug("[Android.startActivityCounter]");
    Android.resetActivityCounter();
    monitorAndroidActivities = true;
};

/**
 * Stop the Android activity counter
 */
Android.stopActivityCounter = function() {
    Android.debug && console.debug("[Android.stopActivityCounter]");
    monitorAndroidActivities = false;
};

/**
 * reset the Android activity counter to 0
 */
Android.resetActivityCounter = function() {
    Android.debug && console.debug("[Android.resetActivityCounter]");
    Android.activeActivities = 0;
};

/**
 * Increment the Android activity count. If the waitTimer is running, cancel it
 */
Android.addActiveActivity = function() {
    if (monitorAndroidActivities) {
        if (waitTimer) {
            // Android.debug && console.debug("[Android.addActiveActivity] cancelling 0 activities event timer");
            clearTimeout(waitTimer);
            waitTimer = null;
        }
        Android.activeActivities++;
        // Android.debug && console.debug("[Android.addActiveActivity] Adding active activity. New active activity total: "+Android.activeActivities);
    }
};

/**
 *  Increment the Android activity count. If the count is less than 1, start the wait timer to
 *  see if another activity starts. If it doesn't fire an app-wide android:noactivities event
 */
Android.removeActiveActivity = function() {
    if (monitorAndroidActivities) {
        Android.activeActivities--;
        // Android.debug && console.debug("[Android.removeActiveActivity] Removing active activity. New active activity total: "+Android.activeActivities);

        if (Android.activeActivities < 1) {
            // Android.debug && console.debug("[Android.removeActiveActivity] !!! No active activites. Firing 0 activities event in 2 seconds...");
            waitTimer = setTimeout(function(){
                // Android.debug && console.debug("[Android.removeActiveActivity] firing android:noactivities event...");
                Ti.App.fireEvent("android:noactivities");
            }, 2000);
        }
    }
};

/**
 * Add Android activity lifecycle listeners to the given Android activity
 *
 * @param    {Object}    activity    Android activity (Ti.Android.Activity)
 */
Android.addAndroidActivityListeners = function(activity) {
    Android.debug && console.debug("[Android.addAndroidActivityListeners]");

    activity.addEventListener("pause", Android.handleAndroidPause);
    activity.addEventListener("resume", Android.handleAndroidResume);
    activity.addEventListener("start", Android.handleAndroidStart);
    activity.addEventListener("stop", Android.handleAndroidStop);
    activity.addEventListener("destroy", Android.handleAndroidDestroy);
};

/**
 * remove Android activity lifecycle listeners from the given Android activity
 *
 * @param    {Object}    activity    Android activity (Ti.Android.Activity)
 */
Android.removeAndroidActivityListeners = function(activity) {
    Android.debug && console.debug("[Android.removeAndroidActivityListeners]");

    activity.removeEventListener("pause", Android.handleAndroidPause);
    activity.removeEventListener("resume", Android.handleAndroidResume);
    activity.removeEventListener("start", Android.handleAndroidStart);
    activity.removeEventListener("stop", Android.handleAndroidStop);
    activity.removeEventListener("destroy", Android.handleAndroidDestroy);
};

/**
 * Increment the activity count when an activity resumes
 */
Android.handleAndroidResume = function() {
    Android.debug && console.debug("[Android.handleAndroidResume] ACTIVITY RESUMED");
    monitorAndroidActivities && Android.addActiveActivity();
};

/**
 * Decrement the activity count when an activity pauses
 */
Android.handleAndroidPause = function() {
    Android.debug && console.debug("[Android.handleAndroidPause] ACTIVITY PAUSED");
    monitorAndroidActivities && Android.removeActiveActivity();
};

/**
 * Log an Android activity destroy event, if debugging is enabled
 */
Android.handleAndroidDestroy = function() {
    Android.debug && console.debug("[Android.handleAndroidDestroy] ACTIVITY DESTROYED");
};

/**
 * Log an Android activity start event, if debugging is enabled
 */
Android.handleAndroidStart = function() {
    Android.debug && console.debug("[Android.handleAndroidStart] ACTIVITY STARTED");
};

/**
 * Log an Android activity stop event, if debugging is enabled
 */
Android.handleAndroidStop = function() {
    Android.debug && console.debug("[Android.handleAndroidStop] ACTIVITY STOPPED");
};

module.exports = Android;